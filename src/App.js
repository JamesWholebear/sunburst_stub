import { useEffect } from "react";
import './App.css';
import define from "./d3script.js";
import {Runtime, Inspector} from "@observablehq/runtime";
import styled from "styled-components";

const Container = styled.div`
  max-width: 800px;
`;

function App() {
  useEffect(() => {
    const runtime = new Runtime();
    runtime.module(define, Inspector.into(document.getElementById('thing')));
  }, [])

  return (
    <Container id="thing">
    </Container>
  );
}

export default App;
